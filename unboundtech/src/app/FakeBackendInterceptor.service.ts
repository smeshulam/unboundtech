import { Injectable } from '@angular/core';
import { HttpRequest, HttpResponse, HttpHandler, HttpEvent, HttpInterceptor, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { delay, mergeMap, materialize, dematerialize } from 'rxjs/operators';

// array in local storage for registered secrets
let secrets = JSON.parse(localStorage.getItem('secrets')) || [];

@Injectable()
export class FakeBackendInterceptor implements HttpInterceptor {
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const { url, method, headers, body } = request;

        // wrap in delayed observable to simulate server api call
        return of(null)
            .pipe(mergeMap(handleRoute))
            .pipe(materialize()) // call materialize and dematerialize to ensure delay even if an error is thrown (https://github.com/Reactive-Extensions/RxJS/issues/648)
            .pipe(delay(500))
            .pipe(dematerialize());

        function handleRoute() {
            switch (true) {
                case url.endsWith('/secrets') && method === 'GET':
                    return getSecrets();
                case url.endsWith('/secrets') && method === 'POST':
                    return createSecret();
                case url.match(/\/secrets\/(.*)/) && method === 'GET':
                    return getSecret();
                case url.match(/\/secrets\/(.*)/) && method === 'PUT':
                    return updateSecret();
                case url.match(/\/secrets\/(.*)/) && method === 'DELETE':
                    return deleteSecret();
                default:
                    // pass through any requests not handled above
                    debugger;
                    return next.handle(request);
            }
        }

        // route functions

        function createSecret() {
            const secret = body

            if (secrets.find(x => x.name === secret.name)) {
              return throwError({ status: 409, error: { message: 'an existing secret with that name already exists' } });
            }

            secret.id = uuidv4();
            secret.createdAt = new Date();
            secrets.push(secret);
            localStorage.setItem('secrets', JSON.stringify(secrets));

            return of(new HttpResponse({ status: 201, body }))
        }

        function updateSecret() {
            const secret = body

            let secretToUpdate = secrets.find(x => x.id === secret.id); 
            const index = secrets.indexOf(secretToUpdate);
            secrets[index] = secret;

            localStorage.setItem('secrets', JSON.stringify(secrets));

            return of(new HttpResponse({ status: 201, body }))
        }

        function getSecrets() {
            const secretsResponse = secrets.map((secret) => {
                return {name: secret.name, id: secret.id, createdAt: secret.createdAt};
            });
            return ok(secretsResponse);
        }

         function getSecret() {
             const secret = secrets.find(x => x.id == idFromUrl());
             if(secret) {
             return ok(secret);
             }
             return throwError({ status: 404, error: { message: 'secret not found' } });
         }

         function deleteSecret() {
             secrets = secrets.filter(x => x.id !== idFromUrl());
             localStorage.setItem('secrets', JSON.stringify(secrets));
             return ok();
         }

        // // helper functions

        function ok(body?) {
            return of(new HttpResponse({ status: 200, body }))
        }


        function idFromUrl() {
            const urlParts = url.split('/');
            return urlParts[urlParts.length - 1];
        }

        function uuidv4() {
            return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
              var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
              return v.toString(16);
            });
          }
          
    }
}

export const fakeBackendProvider = {
    // use fake backend in place of Http service for backend-less development
    provide: HTTP_INTERCEPTORS,
    useClass: FakeBackendInterceptor,
    multi: true
};