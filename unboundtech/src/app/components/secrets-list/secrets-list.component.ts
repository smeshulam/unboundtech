import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Data, Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { ApiService } from 'src/app/api.service';
import { Secret } from 'src/app/models/Secret.model';
import { ResolvedSecretsList } from 'src/app/models/resolvedSecretsList.model';


@Component({
  selector: 'app-secrets-list',
  templateUrl: './secrets-list.component.html',
  styleUrls: ['./secrets-list.component.css']
})
export class SecretsListComponent implements OnInit {
  noSecrets = true;
  private filterdSecrets:Secret[] = []
  secrets: Secret[];
  error: string;

  constructor(private apiService: ApiService, private route: ActivatedRoute, private router: Router, private flashMessage: FlashMessagesService) { }

  ngOnInit() {
    this.route.data.subscribe((data: Data) => {
      const resolvedData: ResolvedSecretsList = data['secrets'];
      if (resolvedData.error === null) {
        this.secrets = resolvedData.secrets;
        this.filterdSecrets = this.secrets;
        if (this.secrets.length === 0) {
          this.noSecrets = false;
        }
      } else {
        this.error = resolvedData.error;
      }
    })
  }

  goToSecret(id: string) {
    this.router.navigate([`${id}`])
  }

  deleteSecret(id: string) {
    this.apiService.deleteSecret(id).subscribe(() => {
      this.secrets = this.secrets.filter(x => x.id !== id);
      this.filterdSecrets = this.secrets;
      this.flashMessage.show('Secret Removed', { cssClass: 'alert-success', timeout: 4000 });
    })
  }

  search(e: Event) {
    const q = (<HTMLInputElement>e.target).value;
    if(q === '') {
      this.filterdSecrets = this.secrets;
      } else {
        this.filterdSecrets = this.secrets.filter(secret => {
          return secret.name.includes(q);
        })
      }
  }

}
