import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Data, Router } from '@angular/router';
import { Secret } from 'src/app/models/Secret.model';
import { ResolvedSecret } from 'src/app/models/resolvedSecret.model';
import { FlashMessagesService } from 'angular2-flash-messages';


@Component({
  selector: 'app-secret',
  templateUrl: './secret.component.html',
  styleUrls: ['./secret.component.css']
})
export class SecretComponent implements OnInit {
  secret: Secret;

  constructor(private route: ActivatedRoute, private flashMessage: FlashMessagesService, private router: Router) { }

  ngOnInit() {
    this.route.data.subscribe((data: Data) => {
      const resolvedData: ResolvedSecret = data['secret'];
      if (resolvedData.error === null) {
        this.secret = resolvedData.secrets;
      } else {
        this.flashMessage.show(resolvedData.error, {cssClass: 'alert-danger', timeout: 4000});
        this.router.navigate(['/not-found']);
      }
    })
  }

}
