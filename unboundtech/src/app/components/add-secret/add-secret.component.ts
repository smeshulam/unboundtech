import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';


import { Router } from '@angular/router';

import { FlashMessagesService } from 'angular2-flash-messages';
import { ApiService } from 'src/app/api.service';
import { NewSecret } from 'src/app/models/NewSecret.model';


@Component({
  selector: 'app-add-secret',
  templateUrl: './add-secret.component.html',
  styleUrls: ['./add-secret.component.css']
})
export class AddSecretComponent implements OnInit {
  allowExport: boolean = false;

  constructor(private apiService: ApiService, private router: Router, private flashMessage: FlashMessagesService) { }

  ngOnInit() {
  }

  onSubmit(f: NgForm) {
    const secret:NewSecret = f.value;
    secret.allowExport = this.allowExport;
    this.apiService.addSecret(secret).subscribe((data) => {
      this.flashMessage.show('Secret Created', {cssClass: 'alert-success', timeout: 4000});
      this.router.navigate(['/']);
    }, error => {
      this.flashMessage.show(error, {cssClass: 'alert-danger', timeout: 4000});
    })
  }

}
