import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/api.service';
import { ActivatedRoute, Data, Router } from '@angular/router';

import { FlashMessagesService } from 'angular2-flash-messages';
import { NgForm } from '@angular/forms';
import { Secret } from 'src/app/models/Secret.model';
import { ResolvedSecret } from 'src/app/models/resolvedSecret.model';

@Component({
  selector: 'app-edit-secret',
  templateUrl: './edit-secret.component.html',
  styleUrls: ['./edit-secret.component.css']
})
export class EditSecretComponent implements OnInit {
  secret: Secret;

  constructor(private route: ActivatedRoute, private apiService: ApiService, private flashMessage: FlashMessagesService, private router: Router) { }

  ngOnInit() {
    this.route.data.subscribe((data: Data) => {
      const resolvedData: ResolvedSecret = data['secret'];
      if (resolvedData.error === null) {
        this.secret = resolvedData.secrets;
      } else {
        this.flashMessage.show(resolvedData.error, {cssClass: 'alert-danger', timeout: 4000});
        this.router.navigate(['/not-found']);
      }
    })
  }

  onSubmit(f: NgForm) {
    this.apiService.updateSecret(f.value).subscribe((data) => {
      this.flashMessage.show('Secret Updated', { cssClass: 'alert-success', timeout: 4000 });
      this.router.navigate(['/']);
    }, error => {
      this.flashMessage.show(error, { cssClass: 'alert-danger', timeout: 4000 });
    })
  }

}
