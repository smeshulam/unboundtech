import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SecretsListComponent } from './components/secrets-list/secrets-list.component';
import { AddSecretComponent } from './components/add-secret/add-secret.component';
import { SecretsListResolver } from './resolvers/secrest-list.resolver.service';
import { SecretComponent } from './components/secret/secret.component';
import { SecretResolver } from './resolvers/secret.resolver.service';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { EditSecretComponent } from './components/edit-secret/edit-secret.component';

const routes: Routes = [
  { path: '', component: SecretsListComponent, resolve: { secrets: SecretsListResolver } },
  { path: 'add-secret', component: AddSecretComponent },
  { path: 'not-found', component: NotFoundComponent},
  { path: ':secretId', component: SecretComponent, resolve: { secret: SecretResolver } },
  { path: 'edit-secret/:secretId', component: EditSecretComponent, resolve: { secret: SecretResolver } },
  {path: '**', redirectTo: '/not-found'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
