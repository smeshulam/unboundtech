export class Secret {
  constructor(public id: string, public createdAt:Date, public name:string, public allowExport?:boolean, public text?: string) { }
}