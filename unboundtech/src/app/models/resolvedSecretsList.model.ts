import { Secret } from './Secret.model';

export class ResolvedSecretsList { 
  constructor(public secrets: Secret[], public error: any = null) {}
}