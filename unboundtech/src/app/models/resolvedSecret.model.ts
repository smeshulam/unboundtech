import { Secret } from './Secret.model';

export class ResolvedSecret {
  constructor(public secrets: Secret, public error: any = null) { }
}