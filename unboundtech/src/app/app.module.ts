import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { ErrorInterceptor } from './_helpers/error.interceptor';
import { fakeBackendProvider } from './FakeBackendInterceptor.service';
import { AddSecretComponent } from './components/add-secret/add-secret.component';
import { FormsModule } from '@angular/forms';
import { SecretsListComponent } from './components/secrets-list/secrets-list.component'
import { SecretsListResolver } from './resolvers/secrest-list.resolver.service';
import { SecretComponent } from './components/secret/secret.component';
import { SecretResolver } from './resolvers/secret.resolver.service';

import { FlashMessagesModule } from 'angular2-flash-messages';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { EditSecretComponent } from './components/edit-secret/edit-secret.component';

@NgModule({
  declarations: [
    AppComponent,
    AddSecretComponent,
    SecretsListComponent,
    SecretComponent,
    NotFoundComponent,
    EditSecretComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    FlashMessagesModule.forRoot()
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    SecretsListResolver,
    SecretResolver,
    // provider used to create fake backend
    fakeBackendProvider
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
