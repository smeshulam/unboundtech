import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { Secret } from './models/Secret.model';
import { NewSecret } from './models/NewSecret.model';



@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private serverUrl: string = 'http://localhost:3000';

  constructor(private http: HttpClient) { }

  getSecrets(): Observable<Secret[]> {
    return this.http.get<Secret[]>(`${this.serverUrl}/secrets`);
  }

  addSecret(secret: NewSecret): Observable<any> {
    return this.http.post<NewSecret>(`${this.serverUrl}/secrets`, secret)
  }

  updateSecret(secret: Secret): Observable<any> {
    return this.http.put<NewSecret>(`${this.serverUrl}/secrets/${secret.id}`, secret)
  }

  getSecret(id: string): Observable<Secret> {
    return this.http.get<Secret>(`${this.serverUrl}/secrets/${id}`);
  }

  deleteSecret(id: string): Observable<any> {
    return this.http.delete<Secret>(`${this.serverUrl}/secrets/${id}`);
  }

}