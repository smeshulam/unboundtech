import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable, of } from 'rxjs';
import { ApiService } from '../api.service';
import { Secret } from '../models/Secret.model';

import { map, catchError } from 'rxjs/operators';
import { ResolvedSecretsList } from '../models/resolvedSecretsList.model';


@Injectable()
export class SecretsListResolver implements Resolve<Observable<ResolvedSecretsList>> {
  constructor(private apiService: ApiService) {}

  resolve(): Observable<ResolvedSecretsList> {
    return this.apiService.getSecrets().pipe(
      map((secrets: Secret[]) => new ResolvedSecretsList(secrets)),
      catchError(err => of(new ResolvedSecretsList(null, err)))
      );
  }
}