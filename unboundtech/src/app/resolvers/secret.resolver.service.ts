import { Injectable } from '@angular/core';

import { Resolve, ActivatedRouteSnapshot } from '@angular/router';

import { Observable, of } from 'rxjs';
import { ApiService } from '../api.service';
import { ResolvedSecret } from '../models/resolvedSecret.model';
import { map, catchError } from 'rxjs/operators';
import { Secret } from '../models/Secret.model';


@Injectable()
export class SecretResolver implements Resolve<Observable<ResolvedSecret>> {
  constructor(private apiService: ApiService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ResolvedSecret> {
    return this.apiService.getSecret(route.params['secretId']).pipe(
      map((secret: Secret) => new ResolvedSecret(secret)),
      catchError(err => of(new ResolvedSecret(null, err)))
      );
  }
}